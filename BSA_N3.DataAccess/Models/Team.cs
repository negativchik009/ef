﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BSA_N3.DataAccess.Abstractions;

namespace BSA_N3.DataAccess.Models
{
    public class Team : IEntity
    {
        public int Id { get; set; }
        [MaxLength(60)]
        [Required]
        public string Name { get; set; }
        [Column("CreationDate")]
        [Required]
        public DateTime? CreatedAt { get; set; }
    }
}