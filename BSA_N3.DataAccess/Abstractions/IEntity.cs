﻿namespace BSA_N3.DataAccess.Abstractions
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}