﻿namespace BSA_N3.WebApi.DTO
{
    public class ProjectInfo
    {
        public ProjectDto Project { get; set; }

        public TaskDto LongestByDescription { get; set; }

        public TaskDto ShortestByName { get; set; }

        public int TeamUsersCount { get; set; }
    }
}