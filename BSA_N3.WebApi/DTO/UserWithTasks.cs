﻿using System.Collections.Generic;

namespace BSA_N3.WebApi.DTO
{
    public class UserWithTasks
    {
        public UserDto User { get; set; }
        public IEnumerable<TaskDto> Tasks { get; set; }
    }
}