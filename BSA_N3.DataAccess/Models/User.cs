﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using BSA_N3.DataAccess.Abstractions;

namespace BSA_N3.DataAccess.Models
{
    public class User : IEntity
    {
        public int Id { get; set; }
        public Team Team { get; set; }
        [Column("Name")]
        [Required]
        public string FirstName { get; set; }
        [Column("Surname")]
        [Required]
        public string LastName { get; set; }
        public string Email { get; set; }
        [Required]
        public DateTime? RegisteredAt { get; set; }
        public DateTime? BirthDay { get; set; }
    }
}