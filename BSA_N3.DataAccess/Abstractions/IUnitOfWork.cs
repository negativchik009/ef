﻿namespace BSA_N3.DataAccess.Abstractions
{
    public interface IUnitOfWork
    {
        public IRepository<TEntity> Set<TEntity>() where TEntity : class, IEntity;
        public void SaveChanges();
        public void SaveChangesAsync();
    }
}