﻿using System;
using System.ComponentModel.DataAnnotations;
using BSA_N3.DataAccess.Abstractions;

namespace BSA_N3.DataAccess.Models
{
    public class Task : IEntity
    {
        public int Id { get; set; }
        public Project Project { get; set; }
        public User Performer { get; set; }
        [Required]
        [MaxLength(45)]
        [MinLength(2)]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public TaskState State { get; set; }
        [Required]
        public DateTime? CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}