﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BSA_N3.Client.Interfaces;
using BSA_N3.Client.Models;
using Newtonsoft.Json;
using Task = BSA_N3.Client.Models.Task;

namespace BSA_N3.Client
{
    public class HttpDataGetter : IDataGetter
    {
        private readonly HttpClient _client;
        
        private readonly Dictionary<Route, string> _routes;

        public HttpDataGetter(Dictionary<Route, string> routes, HttpClient client)
        {
            _routes = routes;
            _client = client;
        }

        public async Task<Dictionary<Project, int>> TasksForUserByProject(int userId)
        {
            return (JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<Project, int>>>(
                    await _client.GetStringAsync(_routes[Route.Selection1])) ?? throw new InvalidOperationException())
                .ToDictionary(n => n.Key,
                    n => n.Value);
        }

        public async Task<IEnumerable<Task>> TasksByUserWithShortNames(int userId)
        {
            return JsonConvert.DeserializeObject<IEnumerable<Task>>(
                await _client.GetStringAsync(_routes[Route.Selection2] + userId));
        }

        public async Task<IEnumerable<TaskIdNamePair>> TasksFinishedInCurrentYear(int userId)
        {
            return JsonConvert.DeserializeObject<IEnumerable<TaskIdNamePair>>(
                await _client.GetStringAsync(_routes[Route.Selection3] + userId));
        }

        public async Task<IEnumerable<TeamInfo>> TeamInfos()
        {
            return JsonConvert.DeserializeObject<IEnumerable<TeamInfo>>(
                await _client.GetStringAsync(_routes[Route.Selection4]));
        }

        public async Task<IEnumerable<User>> SortedUsers()
        {
            return JsonConvert.DeserializeObject<IEnumerable<User>>(
                await _client.GetStringAsync(_routes[Route.Selection5]));
        }

        public async Task<UserInfo> UserInfo(int userId)
        {
            return JsonConvert.DeserializeObject<UserInfo>(
                await _client.GetStringAsync(_routes[Route.Selection6] + userId));
        }

        public async Task<IEnumerable<ProjectInfo>> ProjectInfos()
        {
            return JsonConvert.DeserializeObject<IEnumerable<ProjectInfo>>(
                await _client.GetStringAsync(_routes[Route.Selection7]));
        }
    }
}