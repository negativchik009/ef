﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using BSA_N3.DataAccess.Abstractions;

namespace BSA_N3.DataAccess.Models
{
    public class Project : IEntity
    {
        public int Id { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
        [Column("Title")]
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}