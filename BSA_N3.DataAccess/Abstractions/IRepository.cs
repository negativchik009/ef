﻿using System;
using System.Collections.Generic;

namespace BSA_N3.DataAccess.Abstractions
{
    public interface IRepository<TEntity> where TEntity: IEntity
    {
        public void Create(TEntity entity);
        public IEnumerable<TEntity> Get(Func<TEntity, bool> filter = null);
        public void Update(TEntity entity);
        public void Delete(TEntity entity);
    }
}