﻿using System;
using BSA_N3.DataAccess.Abstractions;

namespace BSA_N3.WebApi.DTO
{
    public class ProjectDto
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}