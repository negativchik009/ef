﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BSA_N3.DataAccess.Abstractions;
using BSA_N3.DataAccess.Models;
using BSA_N3.WebApi.DTO;
using BSA_N3.WebApi.Interfaces;

namespace BSA_N3.WebApi.Services
{
    public class DataProcessor : IDataProcessor
    {
        private readonly IMapper _mapper;

        private readonly IUnitOfWork _data;
        
        public DataProcessor(IUnitOfWork data, IMapper mapper)
        {
            _data = data;
            _mapper = mapper;
        }

        public  IEnumerable<ProjectDto> GetProjects(Func<Project, bool> filter = null)
        {
            return _mapper.Map<IEnumerable<Project>, IEnumerable<ProjectDto>>(_data.Set<Project>().Get(filter));
        }

        public void Create(ProjectDto projectDto)
        {
            _data.Set<Project>().Create(_mapper.Map<ProjectDto, Project>(projectDto));
            _data.SaveChanges();
        }

        public void Update(ProjectDto projectDto)
        {
            _data.Set<Project>().Update(_mapper.Map<ProjectDto, Project>(projectDto));
            _data.SaveChanges();
        }

        public void Delete(ProjectDto projectDto)
        {
            _data.Set<Project>().Delete(_mapper.Map<ProjectDto, Project>(projectDto));
            _data.SaveChanges();
        }

        private Project ProjectFromDto(ProjectDto dto) =>
            new Project()
            {
                Id = dto.Id,
                Name = dto.Name,
                Author = _data.Set<User>().Get(user => user.Id == dto.AuthorId).FirstOrDefault(),
                CreatedAt = dto.CreatedAt,
                Deadline = dto.Deadline,
                Description = dto.Description,
                Team = _data.Set<Team>().Get(team => team.Id == dto.TeamId).FirstOrDefault()
            };

        public IEnumerable<TaskDto> GetTasks(Func<Task, bool> filter = null)
        {
            return _mapper.Map<IEnumerable<Task>, IEnumerable<TaskDto>>(_data.Set<Task>().Get(filter));
        }

        public void Create(TaskDto taskDto)
        {
            _data.Set<Task>().Create(_mapper.Map<TaskDto, Task>(taskDto));
            _data.SaveChanges();
        }

        public void Update(TaskDto taskDto)
        {
            _data.Set<Task>().Update(_mapper.Map<TaskDto, Task>(taskDto));
            _data.SaveChanges();
        }

        public void Delete(TaskDto taskDto)
        {
            _data.Set<Task>().Delete(_mapper.Map<TaskDto, Task>(taskDto));
            _data.SaveChanges();
        }

        private Task TaskFromDto(TaskDto dto) =>
            new Task()
            {
                Id = dto.Id,
                CreatedAt = dto.CreatedAt,
                Description = dto.Description,
                FinishedAt = dto.FinishedAt,
                Name = dto.Name,
                Performer = _data.Set<User>().Get(user => user.Id == dto.PerformerId).FirstOrDefault(),
                Project = _data.Set<Project>().Get(project => project.Id == dto.ProjectId).FirstOrDefault(),
                State = dto.State
            };

        public IEnumerable<TeamDto> GetTeams(Func<Team, bool> filter = null)
        {
            return _mapper.Map<IEnumerable<Team>, IEnumerable<TeamDto>>(_data.Set<Team>().Get(filter));
        }

        public void Create(TeamDto teamDto)
        {
            _data.Set<Team>().Create(_mapper.Map<TeamDto, Team>(teamDto));
            _data.SaveChanges();
        }

        public void Update(TeamDto teamDto)
        {
            _data.Set<Team>().Update(_mapper.Map<TeamDto, Team>(teamDto));
            _data.SaveChanges();
        }

        public void Delete(TeamDto teamDto)
        {
            _data.Set<Team>().Delete(_mapper.Map<TeamDto, Team>(teamDto));
            _data.SaveChanges();
        }

        private Team TeamFromDto(TeamDto dto) =>
            new Team()
            {
                Id = dto.Id,
                CreatedAt = dto.CreatedAt,
                Name = dto.Name
            };

        public IEnumerable<UserDto> GetUsers(Func<User, bool> filter = null)
        {
            return _mapper.Map<IEnumerable<User>, IEnumerable<UserDto>>(_data.Set<User>().Get(filter));
        }

        public void Create(UserDto userDto)
        {
            _data.Set<User>().Create(_mapper.Map<UserDto, User>(userDto));
            _data.SaveChanges();
        }

        public void Update(UserDto userDto)
        {
            _data.Set<User>().Update(_mapper.Map<UserDto, User>(userDto));
            _data.SaveChanges();
        }

        public void Delete(UserDto userDto)
        {
            _data.Set<User>().Delete(_mapper.Map<UserDto, User>(userDto));
            _data.SaveChanges();
        }

        private User UserFromDto(UserDto dto) =>
            new User()
            {
                Id = dto.Id,
                BirthDay = dto.BirthDay,
                Email = dto.Email,
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                RegisteredAt = dto.RegisteredAt,
                Team = _data.Set<Team>().Get(team => team.Id == dto.TeamId).FirstOrDefault()
            };

        public Dictionary<ProjectDto, int> TasksForUserByProject(int userId)
        {
            return GetProjects().ToDictionary(project => project,
                project => 
                    _data.Set<Task>().Get(task => task.Performer.Id == userId) // Do not mapping for productivity
                        .Count(x => x.Project.Id == project.Id));
        }

        public IEnumerable<TaskDto> TasksByUserWithShortNames(int userId)
        {
            const int maxLenght = 45;
            return GetTasks(task => task.Performer.Id == userId && task.Name.Length < maxLenght);
        }

        public IEnumerable<TaskIdNamePair> TasksFinishedInCurrentYear(int userId)
        {
            int currentYear = DateTime.Now.Year;
            return _data.Set<Task>().Get(task => task.FinishedAt?.Year == currentYear)
                .Select(task => new TaskIdNamePair
                {
                    Id = task.Id,
                    Name = task.Name
                });
        }

        public IEnumerable<TeamInfo> TeamInfos()
        {
            const int minAge = 10;
            return GetTeams(team => _data.Set<User>()
                    .Get(user => user.Team.Id == team.Id)
                    .All(user => DateTime.Now.Year - user.BirthDay?.Year > minAge))
                .Select(team => new TeamInfo()
                {
                    Id = team.Id,
                    Name = team.Name,
                    Members = GetUsers(user => user.Team.Id == team.Id)
                        .OrderByDescending(x => x.RegisteredAt)
                })
                .ToList();
        }

        public IEnumerable<UserWithTasks> SortedUsers()
        {
            return GetUsers().OrderBy(user => user.FirstName)
                .Select(user => new UserWithTasks()
                {
                    User = user,
                    Tasks = GetTasks().OrderByDescending(task => task.Name.Length)
                });
        }

        public UserInfo UserInfo(int userId)
        {
            return GetUsers(user => user.Id == userId)
                .Select(user => new UserInfo()
                {
                    User = user,
                    LastProject = _mapper.Map<Project, ProjectDto>(_data.Set<Project>()
                        .Get(project => project.Author.Id == userId)
                        // Alternative with O(n) complexity:
                        // .Aggregate((latest, current) => current.CreatedAt > latest.CreatedAt ? current : latest)) 
                        .OrderBy(project => project.CreatedAt)
                        .Last()),
                    LongestTask = _mapper.Map<Task, TaskDto>(_data.Set<Task>()
                        .Get(task => task.Performer.Id == userId)
                        // Alternative with O(n) complexity:
                        // .Aggregate((longest, current) => 
                        // (current.FinishedAt ?? DateTime.Now) - current.CreatedAt > (longest.FinishedAt ?? DateTime.Now) - longest.CreatedAt ? current : longest) 
                        .OrderBy(task => (task.FinishedAt ?? DateTime.Now) - task.CreatedAt).Last()),
                    LastProjectTasksNumber = _data.Set<Task>().Get(task => task.Project.Id == _data.Set<Project>()
                        .Get(project => project.Author.Id == userId)
                        // Alternative with O(n) complexity:
                        // .Aggregate((latest, current) => current.CreatedAt > latest.CreatedAt ? current : latest)) 
                        .OrderBy(project => project.CreatedAt)
                        .Last().Id)
                        .Count()
                })
                .First();
        }

        public IEnumerable<ProjectInfo> ProjectInfos()
        {
            return GetProjects().Select(project => new ProjectInfo()
            {
                Project = project,
                LongestByDescription = GetTasks(task => task.Project.Id == project.Id)
                    // Alternative with O(n) complexity:
                    // .Aggregate((longest, task) => task.Description.Length > longest.Description.Length ? task : longest)
                    .OrderBy(task => task.Description.Length)
                    .Last(),
                ShortestByName = GetTasks(task => task.Project.Id == project.Id)
                    // Alternative with O(n) complexity:
                    // .Aggregate((shortest, task) => task.Name.Length < shortest.Name.Length ? task : shortest)
                    .OrderBy(task => task.Name)
                    .First(),
                TeamUsersCount = GetTeams(team => team.Id == project.TeamId).Count()
            });
        }
    }
}