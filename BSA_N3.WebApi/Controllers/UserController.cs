﻿using System.Collections.Generic;
using System.Linq;
using BSA_N3.WebApi.DTO;
using BSA_N3.WebApi.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BSA_N3.WebApi.Controllers
{
    public class UserController : ControllerBase
    {
        private readonly IDataProcessor _data;

        public UserController(IDataProcessor data)
        {
            _data = data;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserDto>> Get()
        {
            return new ActionResult<IEnumerable<UserDto>>(_data.GetUsers());
        }

        [HttpGet("{id}")]
        public ActionResult<UserDto> Get(int id)
        {
            var userDto = _data.GetUsers().FirstOrDefault(user => user.Id == id);
            return userDto == null ? BadRequest($"Not found user with id {id}") 
                : new ActionResult<UserDto>(userDto);
        }

        [HttpPost]
        public ActionResult Post(UserDto dto)
        {
            _data.Create(dto);
            return StatusCode(201);
        }

        [HttpPut]
        public ActionResult Put(UserDto dto)
        {
            _data.Update(dto);
            return StatusCode(200);
        }

        [HttpDelete]
        public ActionResult Delete(UserDto dto)
        {
            _data.Delete(dto);
            return StatusCode(204);
        }
    }
}