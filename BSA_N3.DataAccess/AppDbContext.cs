﻿using System;
using System.Collections.Generic;
using BSA_N3.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace BSA_N3.DataAccess
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) :
            base(options)
        {}
        
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var team = new Team
            {
                CreatedAt = new DateTime(2021, 7, 2),
                Id = 1,
                Name = "DreamTeam"
            };

            var user = new User
            {
                BirthDay = new DateTime(2001, 9, 9),
                Email = "SomeEmail@gmail.com",
                FirstName = "Bogdan",
                LastName = "Zaika",
                RegisteredAt = new DateTime(2021, 3, 28),
                Team = team
            };

            var project = new Project
            {
                Author = user,
                CreatedAt = new DateTime(2021, 3, 28),
                Name = "GoodProject",
                Deadline = new DateTime(2021, 9, 15),
                Description = "Some very good project",
                Team = team
            };

            var task = new Task
            {
                CreatedAt = new DateTime(2021, 3, 28),
                Description = "Some very good task for very good project",
                FinishedAt = null,
                Name = "First task",
                Performer = user,
                Project = project,
                State = TaskState.InProgress
            };

            modelBuilder.Entity<Team>().HasData(new List<Team>() {team});
            modelBuilder.Entity<Task>().HasData(new List<Task>() {task});
            modelBuilder.Entity<User>().HasData(new List<User>() {user});
            modelBuilder.Entity<Project>().HasData(new List<Project>() {project});
            
            base.OnModelCreating(modelBuilder);
        }
    }
}