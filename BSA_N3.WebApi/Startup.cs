using AutoMapper;
using BSA_N3.DataAccess;
using BSA_N3.DataAccess.Abstractions;
using BSA_N3.DataAccess.Models;
using BSA_N3.DataAccess.Services;
using BSA_N3.WebApi.DTO;
using BSA_N3.WebApi.Interfaces;
using BSA_N3.WebApi.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace BSA_N3.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();

            var mapper = MapperConfiguration().CreateMapper();
            services.AddTransient(_ => mapper);

            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IDataProcessor, DataProcessor>();
            
            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("LocalDbConnectionString")));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "BSA_N3.WebApi", Version = "v1"});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "BSA_N3.WebApi v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        public MapperConfiguration MapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AllowNullCollections = true;
                
                cfg.CreateMap<Project, ProjectDto>()
                    .ForMember(dto => dto.AuthorId, 
                        opt => opt.MapFrom(
                            project => project.Author.Id))
                    .ForMember(dto => dto.TeamId,
                        opt => opt.MapFrom(
                            project => project.Author.Id));
                cfg.CreateMap<ProjectDto, Project>();

                cfg.CreateMap<Task, TaskDto>()
                    .ForMember(dto => dto.PerformerId,
                        opt => opt.MapFrom(
                            task => task.Performer.Id))
                    .ForMember(dto => dto.ProjectId,
                        opt => opt.MapFrom(
                            task => task.Project.Id));
                cfg.CreateMap<TaskDto, Task>();

                cfg.CreateMap<Team, TeamDto>();
                cfg.CreateMap<TeamDto, Team>();

                cfg.CreateMap<User, UserDto>()
                    .ForMember(dto => dto.TeamId,
                        opt => opt.MapFrom(
                            user => user.Team.Id));
                cfg.CreateMap<UserDto, User>();
            });
            return config;
        }
    }
}